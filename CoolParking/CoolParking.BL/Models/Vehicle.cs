﻿using System;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private const string Simple = "XX-YYYY-XX";
        private const string Chars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        private const string Numbers = "0123456789";

        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ChechingFormatId(id);
            ChecingBalance(balance);
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        private static void ChecingBalance(decimal balance)
        {
            if (balance < 0) throw new ArgumentException("Negative balance");
        }

        private static void ChechingFormatId(string id)
        {
            for (var i = 0; i < Simple.Length; i++)
            {
                for (var j = 0; j < id.Length; j++)
                {
                    if (i != j) continue;
                    if ((Simple[i] == 'X') & !Chars.Contains(id[i])) throw new ArgumentException("Invalid Id");

                    if ((Simple[i] == 'Y') & !Numbers.Contains(id[i])) throw new ArgumentException("Invalid Id");

                    if ((Simple[i] == '-') & (id[i] != '-')) throw new ArgumentException("Invalid Id");
                }
            }
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var identifier = new StringBuilder();

            Random r = new();

            foreach (var item in Simple)
                switch (item)
                {
                    case 'X':
                        identifier.Append(Chars[r.Next(0, Chars.Length)]);
                        break;
                    case 'Y':
                        identifier.Append(Numbers[r.Next(0, Numbers.Length)]);
                        break;
                    default:
                        identifier.Append(item);
                        break;
                }

            return identifier.ToString();
        }
    }
}