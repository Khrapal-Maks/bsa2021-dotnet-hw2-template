﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static readonly Lazy<Parking> Lazy = new(() => new Parking());

        public List<Vehicle> AllTransports { get; }

        public decimal Balance { get; set; }

        public static Parking Source => Lazy.Value;

        private Parking()
        {
            AllTransports = new List<Vehicle>
            {
                Capacity = Settings.Capacity
            };
            Balance = Settings.InitialBalance;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            AllTransports.Clear();
            Balance = 0;
        }

        ~Parking()
        {
            Dispose(false);
        }
    }
}