﻿using System.Timers;

namespace CoolParking.BL.Interfaces
{
    public interface ITimerService
    {
        double Interval { get; set; }
        event ElapsedEventHandler Elapsed;
        void Start();
        void Stop();
        void Dispose();
    }
}