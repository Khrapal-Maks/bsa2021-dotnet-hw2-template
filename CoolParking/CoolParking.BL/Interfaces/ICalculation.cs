﻿using CoolParking.BL.Models;
using System;

namespace CoolParking.BL.Interfaces
{
    public interface ICalculation
    {
        public Parking Parking { get; }

        TransactionInfo WeCalculatePayment(Vehicle vehicle, DateTime dateTime);
    }
}
