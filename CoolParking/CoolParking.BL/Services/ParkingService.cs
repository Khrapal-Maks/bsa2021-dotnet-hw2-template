﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ICalculation _calculationService;
        private readonly ILogService _logService;
        private readonly ITimerService _logTimer;
        private readonly ITimerService _withdrawTimer;

        public List<TransactionInfo> Transactions { get; }

        public Parking Parking { get; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            Parking = Parking.Source;
            Transactions = new List<TransactionInfo>();
            _calculationService = new CalculationService(Parking);
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            GetTimers();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null) throw new ArgumentException("Invalid vehicle");
            if (Parking.AllTransports.Select(x => x.Id).Contains(vehicle.Id)) throw new ArgumentException();
            if (ValidationParkingCapacity())
            {
                Parking.AllTransports.Add(vehicle);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Transport is parked: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(
                    $"\nID:      {vehicle.Id}\nType:    {vehicle.VehicleType}\nBalance: {vehicle.Balance}");
            }
            else
            {
                Console.WriteLine(new InvalidOperationException("Parking lot, no places"));
            }
        }

        public void Dispose()
        {
            Parking.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.AllTransports.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.AllTransports.Capacity - Parking.AllTransports.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new(Parking.AllTransports);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!Parking.AllTransports.Select(x => x.Id).Contains(vehicleId)) throw new ArgumentException("Invalid Id");
            if (Parking.AllTransports.Single(x => x.Id == vehicleId).Balance >= 0)
            {
                Parking.AllTransports.Remove(Parking.AllTransports.Single(x => x.Id == vehicleId));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Transport was issued");
            }
            else
            {
                Console.WriteLine(new InvalidOperationException("Don't remove. Negative balance."));
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0) throw new ArgumentException("Negative sum");
            if (!Parking.AllTransports.Select(x => x.Id).Contains(vehicleId))
            {
                throw new ArgumentException();
            }
            Parking.AllTransports.Single(x => x.Id == vehicleId).Balance += sum;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("The balance has been replenished");
        }

        private void GetTimers()
        {
            _withdrawTimer.Elapsed += WithdrawTimer_WriteOff;
            _withdrawTimer.Start();
            _logTimer.Elapsed += LogTimer_WriteOff;
            _logTimer.Start();
        }

        private void WithdrawTimer_WriteOff(object sender, ElapsedEventArgs e)
        {
            foreach (var itemAllTransport in Parking.AllTransports)
            {
                Transactions.Add(_calculationService.WeCalculatePayment(itemAllTransport, DateTime.Now));
            }
        }

        private void LogTimer_WriteOff(object sender, ElapsedEventArgs e)
        {
            var text = new StringBuilder();

            foreach (var itemTransaction in Transactions)
            {
                text.Append($"~DateTime: {itemTransaction.DateTime}       ID: {itemTransaction.Id}    Sum:{itemTransaction.Sum}");
            }
            _logService.Write(text.ToString());
            Transactions.Clear();
        }

        private bool ValidationParkingCapacity()
        {
            return Parking.AllTransports.Count < Settings.Capacity;
        }
    }
}