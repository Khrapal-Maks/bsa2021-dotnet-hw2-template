﻿using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string _line;

        public string LogPath { get; }

        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public string Read()
        {
            try
            {
                try
                {
                    if (File.Exists(LogPath) == false)
                    {
                        throw new InvalidOperationException();
                    }
                    else
                    {
                        using StreamReader sr = new(LogPath);
                        _line = sr.ReadToEnd();
                    }
                }
                catch
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _line;
        }

        public void Write(string logInfo)
        {
            try
            {
                using StreamWriter sw = new(LogPath, true, Encoding.Default);
                foreach (var item in logInfo.Split(new[] { '~' }))
                {
                    sw.WriteLine(item);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}