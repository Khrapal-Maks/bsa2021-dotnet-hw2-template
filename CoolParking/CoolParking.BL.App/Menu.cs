﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL
{
    public class Menu
    {
        private readonly string _logFilePath = @"Transactions.log.";

        private ParkingService _parkingService;
        private ILogService _logService;
        private TimerService _logTimer;
        private TimerService _withdrawTimer;
        private decimal _sum;
        private string _id;

        public Menu()
        {
            GetServices();
            GetMenuInterface();
        }

        private void GetServices()
        {
            _logTimer = new TimerService
            {
                Interval = Settings.LoggingPeriod
            };
            _withdrawTimer = new TimerService
            {
                Interval = Settings.PaymentChargePeriod
            };
            _logService = new LogService(_logFilePath);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
        }

        private void GetMenuInterface()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Name operation:                                                     Select operation\n" +
                              "                                                                    and enter number. ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(
                "Вывести на экран текущий баланс Парковки:                                  1\n" +
                "Вывести на экран сумму заработанных денег за текущий период:               2\n" +
                "Вывести на экран количество свободных/занятых мест на парковке:            3\n" +
                "Вывести на экран все Транзакции Парковки за текущий период:                4\n" +
                "Вывести историю транзакций:                                                5\n" +
                "Вывести на экран список Тр. средств находящихся на Паркинге:               6\n" +
                "Поставить Тр. средство на Паркинг:                                         7\n" +
                "Забрать транспортное средство с Паркинга:                                  8\n" +
                "Пополнить баланс конкретного Тр. средства:                                 9\n");

            ValidationMenu();
        }

        private void GetOperation(int selection)
        {
            switch (selection)
            {
                case 1:
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Parking balance: {_parkingService.GetBalance()}");
                    ConsoleClearForMenu();
                    break;
                case 2:
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(
                        $"Parking balance for the current period: {_parkingService.Transactions.Sum(x => x.Sum)}");
                    ConsoleClearForMenu();
                    break;
                case 3:
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(
                        $"Free parking spaces: {_parkingService.GetFreePlaces()}\nParking spaces occupied: {Settings.Capacity - _parkingService.GetFreePlaces()}");
                    ConsoleClearForMenu();
                    break;
                case 4:
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Parking transactions for the current period");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("{0,-20} | {1,-15} | {2,-11}", "DateTime", "Id", "Sum");
                    foreach (var item in _parkingService.GetLastParkingTransactions())
                    {
                        Console.WriteLine("{0,-20} | {1,-15} | {2,-11}", item.DateTime, item.Id, item.Sum);
                    }
                    ConsoleClearForMenu();
                    break;
                case 5:
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Transaction history");
                    Console.ForegroundColor = ConsoleColor.White;
                    var log = _parkingService.ReadFromLog() == null ? "Log is empty" : _parkingService.ReadFromLog();
                    foreach (var item in log)
                    {
                        Console.Write(item);
                    }
                    ConsoleClearForMenu();
                    break;
                case 6:
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("All transport  in parking\n");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("{0,-10} | {1,-15} | {2,-11}", "ID", "Type", "Balance");
                    foreach (var item in _parkingService.GetVehicles())
                    {
                        Console.WriteLine("{0,-10} | {1,-15} | {2,-11}", item.Id, item.VehicleType, item.Balance);
                    }
                    ConsoleClearForMenu();
                    break;
                case 7:
                    Console.Clear();
                    _parkingService.AddVehicle(CreateRandomVenicle());
                    ConsoleClearForMenu();
                    break;
                case 8:
                    Console.Clear();
                    if (ValidationId())
                    {
                        _parkingService.RemoveVehicle(_id);
                    }
                    ConsoleClearForMenu();
                    break;
                case 9:
                    Console.Clear();
                    if (ValidationId())
                    {
                        _parkingService.TopUpVehicle(_id, ValidationSum());
                    }
                    ConsoleClearForMenu();
                    break;
                default:
                    Console.Clear();
                    Error();
                    break;
            }
        }

        private static Vehicle CreateRandomVenicle()
        {
            var random = new Random();
            var vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToArray();
            return new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), vehicleTypes[random.Next(0, vehicleTypes.Length)], random.Next(0, 100));
        }

        private void ValidationMenu()
        {
            int selection;

            while (!int.TryParse(Console.ReadLine(), out selection))
            {
                Error();
            }
            GetOperation(selection);
        }

        private decimal ValidationSum()
        {
            Console.WriteLine("Enter sum");
            while (!decimal.TryParse(Console.ReadLine(), out _sum))
            {
                Error();
            }
            return _sum;
        }

        private bool ValidationId()
        {
            Console.WriteLine("Select your Id");
            _id = Console.ReadLine();

            if (!string.IsNullOrWhiteSpace(_id))
            {
                var vehicles = _parkingService.GetVehicles().ToList();
                if (vehicles.Any(itemVehicle => itemVehicle.Id == _id))
                {
                    return true;
                }
            }

            Error();
            return false;
        }

        private void ConsoleClearForMenu()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n\n\nPress 'enter' to return to main menu...");
            Console.ReadKey();
            Console.Clear();
            GetMenuInterface();
        }

        private void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ошибка ввода, проверьте правильность ввода!");
            ConsoleClearForMenu();
        }
    }
}